# 学习使用require.js

## 为什么使用require.js

通常情况下，一个页面可能需要依次加载多个js文件，这种依次加载js文件的方式导致了两个问题:

1. 浏览器会等待js文件的加载，在渲染页面，导致页面假死状态，很不好的用户体验
2. 如果各个js文件只有有依赖性，则必须保证依次顺序一个一个加载，依赖性最大的在最后引入，当依赖性很复杂，代码的编写和维护将变得很困难。

require.js 就是为了解决这样个问题
1. 实现js文件的异步加载，避免页面失去响应
2. 管理模块间的依赖性，便于代码编写和维护

## 使用

在官网下载引入require.js
```
<script src="require.js"></script>
```

加载这个文件，也可能使得网页失去响应, 解决办法(顺道复习一下):
1. 在页面最底部引入js文件
2. 使用defer, 表明该文件需要异步加载，但是IE不支持这个属性，使用async="true"满足IE

加载require.js之后，就需要加载我们自己的js文件，假如我们的js文件命名为 main.js, 则只需要如下写就可以
```
<script src="require.js" defer async="true" data-main="main.js"></script>
```
data-main 属性的意思是指定网页程序的主模块

正常大型应用应该这样引用
```
<script src="require.js"></script> // require.js
<script src="config.js"></script>  // require.config()
<script>
    require(['main']) // main.js 入口文件
</script>
```

### 主模块的写法
```
require(['module1', 'module2'], function (module1, module2) {
    // TODO..
})
```
require接受两个参数第一个参数代表依赖的模块，第二个参数为回调函数。
主模块依赖module1和module2两个模块，当这两个所依赖的模块完全加载完毕之后执行回调函数。

### 模块的加载

通过require.config方法，可以对模块的加载进行自定义。这个方法的参数是一个对象，有三个属性: baseUrl、paths和shim:
1. baseUrl: 配置模块记载的基目录 
2. paths: 该属性也是一个对象，指定各个模块的加载路径
3. ship: 加载不符合AMD规范的模块


```
require.config({
    baseUrl: 'test',
    paths: {
        a: 'a', // test/a.js
        b: 'common/b'   // test/common/b.js
    },
    shim: {
        test: {
            deps: ['jquery'], // 所依赖的模块
            exports: 'testxx' // 输出变量，表明这个模块外部调用的名称
        }
    }
})
```